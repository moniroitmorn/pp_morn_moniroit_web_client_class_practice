package com.example.entity;

import com.example.dto.CourseDto;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long course_id;
	private String courseName;
	private String courseCode;
	private String description;
	private String instructor;

//		public Course(Long id, String courseName, String courseCode, String description, String instructor){
//						this.id=id;
//						this.courseName=courseName;
//						this.courseCode=courseCode;
//						this.description=description;
//						this.instructor=instructor;
//		}

		public CourseDto toDto(){
			return  new CourseDto(this.course_id, this.courseName,this.courseCode,this.description, this.instructor);
		}

}
