package com.example.ServiceImpl;

import com.example.Repository.CourseRepository;
import com.example.Service.CourseService;
import com.example.dto.CourseDto;
import com.example.entity.Course;
import com.example.request.CourseRequest;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

@Service
public class CourseServiceImpl implements CourseService {


	private final CourseRepository courseRepository;

	public CourseServiceImpl(CourseRepository courseRepository) {
		this.courseRepository = courseRepository;
	}

	@Override
	public CourseDto addNewCourse(CourseRequest courseRequest) {

		var courseEntity=courseRequest.toEntity();
		return courseRepository.save(courseEntity).toDto();
	}

	@Override
	public CourseDto getCourseById(Long id) {
		Course course=courseRepository.findById(id).orElseThrow(()->new NotFoundException("Id Not Found"));
		return courseRepository.findById(id).get().toDto();
	}
}
