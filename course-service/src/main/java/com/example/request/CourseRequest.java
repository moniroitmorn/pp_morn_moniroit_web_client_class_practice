package com.example.request;

import com.example.entity.Course;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CourseRequest {
	private String courseName;
	private String courseCode;
	private String description;
	private String instructor;

		public Course toEntity(){
			return new Course(null, this.courseName,this.courseCode,this.description,this.instructor);
		}

}
