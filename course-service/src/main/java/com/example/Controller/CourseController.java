package com.example.Controller;

import com.example.Service.CourseService;
import com.example.dto.CourseDto;
import com.example.request.CourseRequest;
import com.example.response.AllResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/course")
public class CourseController {

	private final CourseService courseService;

	public CourseController(CourseService courseService) {
		this.courseService = courseService;
	}

	@PostMapping("/addData")
	public AllResponse<?>addData(@RequestBody CourseRequest courseRequest){
		var payload= courseService.addNewCourse(courseRequest);
		return AllResponse.<CourseDto>builder()
				.message("Success")
				.payload(payload)
				.status(HttpStatus.OK)
				.build();
	}

	@GetMapping("/{id}")
	public AllResponse<?>getById(@PathVariable("id") Long id){
			var findCourse= courseService.getCourseById(id);
			return AllResponse.<CourseDto>builder()
					.message("Search Successfully")
					.payload(findCourse)
					.status(HttpStatus.OK)
					.build();

	}
}
