package com.example.Service;

import com.example.dto.CourseDto;
import com.example.request.CourseRequest;

public interface CourseService {

	CourseDto addNewCourse(CourseRequest courseRequest);
	CourseDto getCourseById(Long id);

}
