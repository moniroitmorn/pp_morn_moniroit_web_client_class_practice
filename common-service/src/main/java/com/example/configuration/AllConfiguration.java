package com.example.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class AllConfiguration {

	@Bean
	public WebClient.Builder courseServiceWebClientBuilder() {
		return WebClient.builder().baseUrl("http://localhost:8088/api/v1");
	}

	@Bean
	public WebClient.Builder studentServiceWebClientBuilder() {
		return WebClient.builder().baseUrl("http://localhost:8090/api/v1");
	}
}
