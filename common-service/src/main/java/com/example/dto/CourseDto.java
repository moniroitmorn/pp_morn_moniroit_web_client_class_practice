package com.example.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CourseDto {

	private Long course_id;
	private String courseName;
	private String courseCode;
	private String description;
	private String instructor;
}
