package com.example.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class StudentDto {
	private Long student_id;
	private String firstName;
	private String lastName;
	private String email;
	private LocalDateTime dateOfBirth;
	private Long course_id;




}
