package com.example.Controller;

import com.example.ServiceImp.StudentService;
import com.example.dto.StudentDto;
import com.example.request.StudentRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("api/v1/students")
public class StudentController {

	private final StudentService studentService;

	public StudentController(StudentService studentService) {
		this.studentService = studentService;
	}




	@PostMapping
	public ResponseEntity<StudentDto> createStudent(@RequestBody StudentRequest studentRequest,
													@RequestParam Long courseId) {
		StudentDto createdStudent = studentService.createStudent(studentRequest, courseId);
		return ResponseEntity.status(HttpStatus.CREATED).body(createdStudent);
	}



}
