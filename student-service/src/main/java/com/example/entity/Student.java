package com.example.entity;


import com.example.dto.StudentDto;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long student_id;
	private String firstName;
	private String lastName;
	private String email;
	private LocalDateTime dateOfBirth;
	private Long course_id;

	public Student(Object o, String firstName, String lastName, String email, LocalDateTime dateOfBirth, Long courseId, Long courseId1) {
	}


	public StudentDto toDto(){
		return  new StudentDto(this.student_id, this.firstName,this.lastName,this.email, this.dateOfBirth,this.toDto().getCourse_id());
	}

}
