package com.example.ServiceImp;

import com.example.Repository.StudentRepository;
import com.example.dto.CourseDto;
import com.example.dto.StudentDto;
import com.example.entity.Student;
import com.example.request.StudentRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.webjars.NotFoundException;
import reactor.core.publisher.Mono;

import java.util.Random;

@Service

public class StudentService {


	private final WebClient.Builder courseServiceWebClientBuilder;
	private final WebClient.Builder studentServiceWebClientBuilder;

	public StudentService(WebClient.Builder courseServiceWebClientBuilder,
						  WebClient.Builder studentServiceWebClientBuilder) {
		this.courseServiceWebClientBuilder = courseServiceWebClientBuilder;
		this.studentServiceWebClientBuilder = studentServiceWebClientBuilder;
	}


//	private  Long course_id;
	public StudentDto createStudent(StudentRequest studentRequest, Long course_id) {

		CourseDto courseDto = courseServiceWebClientBuilder
				.build()
				.get()
				.uri("/course/{course_id}", course_id)
				.retrieve()
				.bodyToMono(CourseDto.class)
				.block();

		if (courseDto == null) {
			throw new NotFoundException("Course not found");
		}

		// Step 2: Create a new Student record with course information
		// (You can merge data from courseDto and studentDto as needed)
		StudentDto newStudentDto = new StudentDto();
		newStudentDto.setFirstName(studentRequest.getFirstName());
		newStudentDto.setLastName(studentRequest.getLastName());
		newStudentDto.setEmail(studentRequest.getEmail());
		newStudentDto.setDateOfBirth(studentRequest.getDateOfBirth());

		// Additional logic to set course-related data from courseDto in newStudentDto

		// Step 3: Save the new Student record in the Student-service database
		// (Assuming you have a repository and service methods for this)

		// Return the created Student DTO
		return newStudentDto;
	}




}



