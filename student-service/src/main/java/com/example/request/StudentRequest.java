package com.example.request;

import com.example.entity.Student;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class StudentRequest {

	private String firstName;
	private String lastName;
	private String email;
	private LocalDateTime dateOfBirth;
	private Long course_id;


	public Student toEntity(){
		return  new Student(null,this.firstName,this.lastName, this.email,this.dateOfBirth, this.course_id, this.course_id);
	}

}
